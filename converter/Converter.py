import itertools
from abc import ABCMeta, abstractmethod
import json
import PIL.Image as Image


class Converter(object):
    """
    Abstract base class template for conversion to MSCOCO Format.
    For each dataset a child class inheriting this class must be prepared.
    """
    __metaclass__ = ABCMeta

    def __init__(self, name):
        """
        Constructor.
        :param name: Name of dataset.
        """
        self._name = name

    @property
    @abstractmethod
    def categories(self):
        """
        Must return a list of object category names in the dataset.
        :return: A list of object category names
        """
        pass

    @abstractmethod
    def cat2id(self, categoryname):
        """
        Returns the ID of a given object category
        :param categoryname: Name of the object category
        :return: An integer denoting the corresponding ID
        """
        pass

    @property
    @abstractmethod
    def imagelist(self):
        """
        Must return a list of all the image files in the dataset.
        :return: A list of image file names
        """
        pass

    @abstractmethod
    def image2id(self, imagefile):
        """
        Must return the ID of a given image
        :param imagefile: String. The name of the image file .
        :return: An integer denoting the corresponding ID.
        """
        pass

    @abstractmethod
    def _readannotation(self, imagefile):
        """
        Must read an annotation file.
        This function should return a list of dicts. Each dict should have the following form
        { 'bbox' : [xmin, ymin, width, height],
          'category' : Name of the corresponding object category
        }
        :param imagefile: String. The name of the image file.
        :return: A list of dicts in the format as specified above.
        """
        pass

    @staticmethod
    def _getimgdim(imagefilename):
        """
        This staticmethod returns the height and width of an image file.
        :param imagefilename: String. Name of the image file.
        :return: A tuple (height, width)
        """
        im = Image.open(imagefilename)
        height, width = im.size
        return height, width

    def write_coco_style(self, cocofilepath):
        """
        This method should not be overrriden. It prepares the annotations in MSCOCO format and writes them to a JSON file.
        :param cocofilepath: Full path to the JSON file where the data should be written in MSCOCO format.
        :return: None
        """
        jsondata = dict()
        infodict = dict()
        infodict['description'] = self._name
        jsondata['info'] = infodict
        imagelist = [self._imgelement(x) for x in self.imagelist]
        jsondata['images'] = imagelist
        annlist = [self._annelement(x) for x in self.imagelist]
        annlist = list(itertools.chain(*annlist))
        jsondata['annotations'] = annlist
        jsondata['categories'] = [{'id': self.cat2id(x),
                                   'name': x} for x in self.categories]
        with open(cocofilepath, 'w') as outfile:
            json.dump(jsondata, outfile)

    def _imgelement(self, imagefilename):
        """
        Extracts the 'images' component of MSCOCO format. Must not be overriden. It is called internally by
        write_coco_style()
        :param imagefilename: Name of the image file.
        :return: Returns a list.
        """
        element = dict()
        element['file_name'] = imagefilename
        height, width = self._getimgdim(imagefilename)
        element['height'] = height
        element['width'] = width
        id = self.image2id(imagefilename)
        element['id'] = id
        return element

    def _annelement(self, imagefilename):
        """
        Extracts the 'annotations' component of MSCOCO format. Must not be overriden. It is called internally by
        write_coco_style()
        :param imagefilename: Name of the image file.
        :return: Returns a list.
        """
        annlist = self._readannotation(imagefilename)
        id = self.image2id(imagefilename)
        annlist = [{'image_id': id,
                    'id': -1,
                    'bbox': x['bbox'],
                    'segmentation' : [[x['bbox'][0], x['bbox'][2], x['bbox'][0], x['bbox'][3], x['bbox'][1], x['bbox'][3], x['bbox'][1],
                                       x['bbox'][2], x['bbox'][0], x['bbox'][2]]],
                    'area' : float(x['bbox'][2] * x['bbox'][3]),
                    'iscrowd' : 0,
                    'category_id': self.cat2id(x['category'])}
                   for x in annlist]
        return annlist
