import glob
import os

from converter.Converter import Converter


class Caltech2COCO(Converter):
    """
    Class to convert Caltech pedestrian dataset annotations to MSCOCO format
    """
    def __init__(self, name, datadir, anntype='full'):
        """
        Constructor.
        :param name: Name of the dataset (or a subset of it)
        :param datadir: Directory in which the 'annotations' subfolder contains annotations and 'images' subfolder
        contains images.
        """
        super(Caltech2COCO, self).__init__(name)
        self._datadir = datadir
        images = glob.glob(os.path.join(self._datadir, 'images', '*.jpg'), recursive=True)
        annlist = glob.glob(os.path.join(self._datadir, 'annotations',
                                         '*.txt'), recursive=True)
        if anntype == 'full' or anntype == 'vis':
            self._anntype = anntype
        else:
            raise(ValueError, 'anntype must be either "full" or "vis".')
        images = self.arrange(images)
        annlist = self.arrange(annlist)
        ids = range(1, len(images) + 1)
        self._data = {z[0]: {'ann': z[1], 'id': z[2]}
                      for z in zip(images, annlist, ids)}
        self._catlist = {'person': 1}

    @property
    def imagelist(self):
        return list(self._data.keys())

    @property
    def anntype(self):
        return self._anntype

    @property
    def categories(self):
        return list(self._catlist.keys())

    def cat2id(self, id):
        return self._catlist[id]

    def image2id(self, imagefile):
        return self._data[imagefile]['id']

    def image2annfile(self, imagefile):
        return self._data[imagefile]['ann']

    def _readannotation(self, imagefile):
        annfile = self.image2annfile(imagefile)
        annotations = []
        for line in open(annfile, 'r'):
            line = line.strip()
            line = line.split(' ')
            if line[0] == '%':
                continue
            if line[0] == 'person' or line[0] == 'people':
                if self.anntype == 'full':
                    gt = line[1:5]
                else:
                    gt = line[6:10]
                    gt = list(map(float, gt))
                    if all(gt):
                        gt = line[1:5]
                gt = list(map(float, gt))
                annotations.append(
                    {'bbox': gt,
                     'category': 'person'}
                )
        return annotations

    @staticmethod
    def arrange(filelist):
        newlist = sorted(filelist, key=lambda x: os.path.splitext(
            os.path.basename(x)))
        return newlist
