# DB2COCO

#### Description

> This repository contains Python 3.6 code to convert a given dataset to MSCOCO format.

#### Perspective

> Different datasets have their own annotation format. It is of interest to have a common format 
as this can facilitate easier experimentations through a common API. MSCOCO is a popular dataset
and a lot of datasets are already available in MSCOCO format. MSCOCO also comes with its own set of 
tools to extract annotations and conduct evaluations.

>This repository was originally written to convert pedestrian detection datasets into MSCOCO format,
because the [evaluation code in Python for pedestrian detection](https://bitbucket.org/shanshanzhang/citypersons) 
expected annotations and groundtruth data in MSCOCO format. Although [another evaluation code is available for
MATLAB](http://www.vision.caltech.edu/Image_Datasets/CaltechPedestrians/), it is much harder to use and modify for new datasets.


#### Structure
There is an abstract base class called `Converter` which contains the actual code for writing annotations 
in [MSCOCO format](http://cocodataset.org/#format-data). For a specific dataset, a child class should be 
created which inherits `Converter` class and it must ovverride the `abstractmethods` specified in the `Converter` class. The details of the 
`abstractmethods` can be seen in `Converter.py`. 

For an example please see `caltech2coco.py`


#### Usage 

The following snippet is an example usage of the code
(*For a custom class, the name of the class must be changed*)

```python
from converter.caltech2coco import Caltech2COCO

dbpath = '/data/stars/user/uujjwal/datasets/pedestrian/caltech/caltech10x-train'

converter = Caltech2COCO('reasonable-train', dbpath)

converter.write_coco_style('./reasonable-coco.json')
```

